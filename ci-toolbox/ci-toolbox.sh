#!/usr/bin/env bash
# citbx4gitlab: CI toolbox for Gitlab
# Copyright (C) 2017-2018 ERCOM - Emeric Verschuur <emeric@mbedsys.org>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

CITBX_VERSION=$(cat $(dirname $0)/../VERSION)

CITBX_TOOL_DIR=$(dirname $(readlink -f $0))
if [ -f "$CITBX_TOOL_DIR/ci-toolbox.properties" ]; then
    . "$CITBX_TOOL_DIR/ci-toolbox.properties"
fi
. "$CITBX_TOOL_DIR/common.sh"

citbx_env_init

# Check minimum version if required
if [ -n "$CITBX_VERSION_REQ_MIN" ] && [ "$1" != "setup" ] \
    && [ $(printf "%d%04d%04d" ${CITBX_VERSION//[^0-9]/ }) \
        -lt $(printf "%d%04d%04d" ${CITBX_VERSION_REQ_MIN//[^0-9]/ }) ]; then
    print_warning "This project require CI-Toolbox version $CITBX_VERSION_REQ_MIN or greater" \
                "This tool will be upgraded to the last available version" \
                "You can abort this process and manually update this tool using 'ci-toolbox setup --component ci-toolbox <specific version>"
    echo -n "Type [ENTER] to continue or [CTRL]+[C] to abort."
    read
    exec ci-toolbox setup --component ci-toolbox
    exit 1
fi

# Collect OS release information
declare -A OS_RELEASE_INFO
eval "$(sed -E 's/^([^=]+)=(.*)$/OS_RELEASE_INFO[\1]=\2/g' /etc/os-release)"

# YAML to JSON convertion
citbx_yaml2json() {
    if [ ${#CITBX_MISSING_PKGS[@]} -ne 0 ]; then
        print_critical "System setup required (command 'ci-toolbox setup')"
    fi
    cat "$@" | python3 -c 'import sys, yaml, json; json.dump(yaml.load(sys.stdin), sys.stdout)'
}

# Collect the missing binaries and other dependencies
CITBX_MISSING_PKGS=()
for bin in gawk jq dockerd; do
    if ! which $bin > /dev/null 2>&1; then
        CITBX_MISSING_PKGS+=($bin)
    fi
done
if [ "$(echo "true" | citbx_yaml2json 2>/dev/null)" != "true" ]; then
    CITBX_MISSING_PKGS+=("python-yaml")
fi
if [ "$CITBX_GIT_LFS_SUPPORT_ENABLED" == "true" ] && ! git lfs version > /dev/null 2>&1; then
    CITBX_MISSING_PKGS+=("git-lfs")
fi

citbx_gitlab_ci_query() {
    jq "$@" <<< "$CITBX_GITLAB_CI_JSON"
}

citbx_pipelines_query() {
    jq "$@" <<< "$CITBX_PIPELINES_JSON"
}

# Check environment and run setup
citbx_check_env() {
    if [ "$1" != "true" ]; then
        if [ ${#CITBX_MISSING_PKGS[@]} -gt 0 ]; then
            print_critical "System setup needed (binary(ies)/component(s) '${CITBX_MISSING_PKGS[*]}' missing): please execute 'ci-toolbox setup' first"
        fi
        return 0
    fi
    local setupsh="$CITBX_TOOL_DIR/env-setup/${CITBX_SETUP_OS_ID,,}.sh"
    if [ ! -f "$setupsh" ]; then
        print_critical "OS variant '$CITBX_SETUP_OS_ID' not supported. You can:" "* Write the suitable setup script $setupsh" \
                        "* Use --os-id option to use setup script from an other similar OS"
    fi
    check_dns() {
        case "$1" in
            ::1|127.*)
                print_error "Local $1 DNS server cannot be used with docker containers"
                return 1
                ;;
            *)
                echo "$1"
                ;;
        esac
    }
    setup_component_enabled() {
        local pattern='\b'"$1"'\b'
        if [[ "${CITBX_SETUP_COMPONENT[*]}" =~ $pattern ]]; then
            return 0
        fi
        return 1
    }
    bashopts_process_option -n CITBX_DOCKER_DNS_LIST -r -k check_dns
    . "$setupsh"
    print_info "System setup complete" "On a first install, a system reboot may be necessary"
    exit 0
}

# Get the job list
citbx_job_list() {
    local prefix outcmd arg
    outcmd='print $0'
    if ! arglist=$(getopt -o "f:p:s" -n "citbx_list " -- "$@"); then
        print_critical "Usage citbx_list: [options]" \
            "        -f <val>  Output gawk command (default: 'print $0')" \
            "        -s        Suffix list (same as -f 'printf(\" %s\", f[1]);')" \
            "        -p <val>  Prefix string"
    fi
    eval set -- "$arglist";
    while true; do
        arg=$1
        shift
        case "$arg" in
            -f) outcmd=$1;  shift;;
            -p) prefix=$1;  shift;;
            -s) outcmd='printf(" %s", f[1]);';;
            --) break;;
            *)  print_critical "Fatal error";;
        esac
    done
    printf "%s\n" "${CITBX_JOB_LIST[@]}" \
        | gawk 'match($0, /^'"$prefix"'(.*)$/, f) {'"$outcmd"'}'
}

# Run job dependencies
citbx_run_job_dependencies() {
    local job_dependency_tree
    # Build job dependency graph
    citbx_fill_dependency_tree() {
        local job_name=$1
        if printf "%s\n" "${job_dependency_tree[@]}" | grep -q ^"$job_name"$; then
            # Job already in the list: nothing to do
            return
        fi
        job_dependency_tree+=("$job_name")
        if [ $(citbx_gitlab_ci_query -r '."'"$job_name"'".dependencies | length') -eq 0 ]; then
            # Job without dependencies: nothing to do
            return
        fi
        while read -r j; do
            citbx_fill_dependency_tree "$j"
        done <<< "$(citbx_gitlab_ci_query -r '."'"$job_name"'".dependencies[]')"
    }
    citbx_fill_dependency_tree "$CI_JOB_NAME"
    if [ ${#job_dependency_tree[@]} -eq 1 ]; then
        print_note "No dependency found."
        return
    fi
    local subjob_list
    local env_args
    while read -r j; do
        subjob_list+=("$j")
    done <<< "$(printf "%s\n" "${job_dependency_tree[@]:1}" | tac)"
    for v in "${CITBX_SUBJOB_ENV_EXPORT_LIST[@]}"; do
        env_args+=("$v=${!v}")
    done
    print_info "Number of dependencies: ${#subjob_list[@]}"
    for j in "${subjob_list[@]}"; do
        env "${env_args[@]}" $0 "$j"
    done
    print_info "All dependency jobs executed"
}

# DEPRECATED: Run an other job
citbx_run_ext_job() {
    print_warning "The 'citbx_run_ext_job' function is deprecated since version 5.2.0 and will be removed in a future release." \
                    "=> Please remove all references to this function in the job_define/job_setup hook from the following file:" \
                    "  * $CITBX_JOBS_DIR/$CITBX_JOB_FILE_NAME" \
                    "=> If you were using this function to run job's dependencies, then use the '--with-dependencies' option instead."
    if [ "$CITBX_RUN_JOB_DEPENDENCIES" == "true" ]; then
        print_critical "'--with-dependencies' option cannot be used with the 'citbx_run_ext_job' function."
    fi
    local env_args
    local env
    for env in "${bashopts_optlist[@]:1}"; do
        case "$env" in
            CITBX_COMMAND|CITBX_JOB_FILE_NAME|CITBX_JOB_RUN_FILE_NAME|CITBX_GIT_CLEAN|CITBX_RUN_SHELL|CITBX_DOCKER_IMAGE)
            ;;
            *)
                env_args+=("$env=${!env}")
            ;;
        esac
    done
    env "${env_args[@]}" "$0" "$@"
}

# Export a variable to the job environment
citbx_export() {
    CITBX_ENV_EXPORT_LIST+=("$@")
}

# Export a variable to the sub jobs
citbx_subjob_export() {
    CITBX_SUBJOB_ENV_EXPORT_LIST+=("$@")
}

# Add docker run arguments
citbx_docker_run_add_args() {
    CITBX_JOB_DOCKER_RUN_ARGS+=("$@")
}

citbx_run_job() {
    # Fetch git submodules
    if [ "$GIT_SUBMODULE_STRATEGY" != "none" ]; then
        GIT_SUBMODULE_ARGS=()
        case "$GIT_SUBMODULE_STRATEGY" in
            normal)
                ;;
            recursive)
                GIT_SUBMODULE_ARGS+=("--recursive")
                ;;
            *)
                print_critical "Invalid value for GIT_SUBMODULE_STRATEGY: $GIT_SUBMODULE_STRATEGY"
                ;;
        esac
        print_info "Fetching git submodules..."
        git submodule --quiet sync "${GIT_SUBMODULE_ARGS[@]}"
        git submodule update --init "${GIT_SUBMODULE_ARGS[@]}"
    fi

    if [ "$CITBX_GIT_CLEAN" == "true" ]; then
        git clean -fdx
        if [ "$GIT_SUBMODULE_STRATEGY" != "none" ]; then
            git submodule --quiet foreach "${GIT_SUBMODULE_ARGS[@]}" git clean -fdx
        fi
    fi

    # Run job dependencies first (if asked)
    if [ "$CITBX_RUN_JOB_DEPENDENCIES" == "true" ]; then
        citbx_run_job_dependencies
    fi

    # Login to the registry if needed
    if [ -n "$CI_REGISTRY" ] \
        && ( ( [ -z "$(jq -r '."auths"."'$CI_REGISTRY'" | select(.auth != null)' $HOME/.docker/config.json 2> /dev/null)" ] \
                && [ "$CITBX_DOCKER_LOGIN_MODE" == "auto" ] ) \
            || [ "$CITBX_DOCKER_LOGIN_MODE" == "enabled" ] ); then
        print_info "You seem to be not authenticated against the gitlab docker registry" \
            "> You can disable this feature by using --docker-login=disabled" \
            "> Or force this feature permanently by setting CITBX_DEFAULT_DOCKER_LOGIN_MODE into $CI_PROJECT_DIR/.ci-toolbox.properties" \
            "Please enter your gitlab user id and password:"
        docker login $CI_REGISTRY
    fi

    # Compute commands from before_script script and after_script
    # ### Script part: BEGIN ###
    CITBX_JOB_SCRIPT_PARTS+=('
        print_info() {
            printf "\033[1m\033[92m%s\033[0m\n" "$@"
        }
        print_error() {
            printf "\033[1m\033[91m%s\033[0m\n" "$@"
        }
        print_cmd() {
            printf "\033[1m\033[92m$ %s\033[0m\n" "$@"
        }
        ')
    if [ "$CITBX_DEBUG_SCRIPT_ENABLED" == "true" ]; then
        CITBX_JOB_SCRIPT_PARTS+=('set -x')
    fi
    # ### Script part ###
    CITBX_JOB_SCRIPT_PARTS+=('
        set -e
        __citbx_on_job_exit__() {
            local __job_exit_code__=$?
            (
                set -e
                print_info "Running after script..."
                '"$(
                # Extract the job specific or global after_script property content
                #   and put it into the job exit function to be executed after the main part
                citbx_gitlab_ci_query -r '
                    if ."'"$CI_JOB_NAME"'".after_script != null then
                        ."'"$CI_JOB_NAME"'".after_script
                    elif .after_script != null then
                        .after_script
                    else [] end | map([@sh "print_cmd \(.)", "\(.)"])
                    | if (. | length) != 0 then (. | add | join("\n")) else "" end
                ')"'
            )
            if [ $__job_exit_code__ -eq 0 ]; then
                print_info "Job succeeded"
            else
                print_error "ERROR: Job failed: exit code $__job_exit_code__"
            fi
        }
        trap __citbx_on_job_exit__ EXIT SIGHUP SIGINT SIGQUIT SIGABRT SIGKILL SIGALRM SIGTERM
        '"$(
        # Extract the content of job specific or global before_script property
        #   following by the job script property content
        citbx_gitlab_ci_query -r '[
        if ."'"$CI_JOB_NAME"'".before_script != null then
            ."'"$CI_JOB_NAME"'".before_script
        elif .before_script != null then
            .before_script
        else [] end, ."'"$CI_JOB_NAME"'".script]
            | add | map([@sh "print_cmd \(.)", "\(.)"])
            | if (. | length) != 0 then (. | add | join("\n")) else "" end'
    )")
    CITBX_JOB_SCRIPT="'${CITBX_JOB_SCRIPT_PARTS[*]//\'/\'\\\'\'}'"

    # Git SHA1
    CI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME:-$(cd $CI_PROJECT_DIR && git rev-parse --abbrev-ref HEAD)}
    CITBX_JOB_DOCKER_RUN_ARGS+=(-e CI_COMMIT_REF_NAME="$CI_COMMIT_REF_NAME")

    # If not set, fill the CI_SERVER_TLS_CA_FILE with local CA certificates
    if ! [[ -v CITBX_TLS_CA_SEARCH_DIR_LIST ]]; then
        CITBX_TLS_CA_SEARCH_DIR_LIST=("/usr/local/share/ca-certificates/")
    fi
    if ! [[ -v CI_SERVER_TLS_CA_FILE ]]; then
        CI_SERVER_TLS_CA_FILE="$(
            for dir in "${CITBX_TLS_CA_SEARCH_DIR_LIST[@]}"; do
                test ! -d "$dir" \
                    || find "$dir" -iregex '.*\.\(pem\|crt\)$' -exec openssl x509 -in '{}' \;
            done
        )"
    fi

    # Add variable to the environment list
    citbx_export CI_JOB_NAME \
                 CI_REGISTRY \
                 CI_PROJECT_DIR \
                 CI_SERVER_TLS_CA_FILE \
                 CITBX_SCRIPTS_DIR \
                 CITBX_MODULES_DIR \
                 CITBX_JOBS_DIR

    if [ "$CITBX_DEBUG_SCRIPT_ENABLED" == "true" ]; then
        citbx_before_script="set -x"
        citbx_after_script="set +x"
    else
        citbx_before_script=""
        citbx_after_script=""
    fi

    # Run the job setup hooks
    for hook in $citbx_job_stage_setup; do
        $citbx_before_script
        $hook
        $citbx_after_script
    done

    CITBX_SCRIPT_COMMON='
        _which() {
            for p in $(echo "$PATH" | tr : \ ); do
                if [ -x "$p/$1" ]; then
                    echo "$p/$1"
                    return 0
                fi
            done
            return 1
        }
        if ! SHELL="$(_which bash || _which sh)"; then
            echo shell not found
            exit 1
        fi
    '

    case "$CITBX_JOB_EXECUTOR" in
        shell)
            print_info "Running the job \"$CI_JOB_NAME\" into the shell..."
            (
                unset CITBX
                export GITLAB_CI=true
                for e in ${CITBX_ENV_EXPORT_LIST[@]}; do
                    export $e
                done
                for e in "${CITBX_JOB_VARIABLE_LIST[@]}"; do
                    export $e
                done
                # Shell selector (like the gitlab-runner one) will try to find bash, otherwise sh
                eval "$CITBX_SCRIPT_COMMON"'$SHELL -c '"$CITBX_JOB_SCRIPT"
            )
            ;;
        docker)
            # Setup docker environment
            if [ -z "$CITBX_DOCKER_IMAGE" ] || [ "$CITBX_DOCKER_IMAGE" == "null" ]; then
                print_critical "No image property found in .gitlab-ci.yml for the job \"$CI_JOB_NAME\""
            fi
            CITBX_ID=$(head -c 8 /dev/urandom | od -t x8 -An | grep -oE '\w+')
            CITBX_DOCKER_PREFIX="citbx-$CITBX_ID"
            case "$CITBX_DOCKER_AUTH_CONFIG_MODE" in
                environment)
                    citbx_export DOCKER_AUTH_CONFIG
                ;;
                mount-ro|mount-rw)
                    if [ -f "$HOME/.docker/config.json" ]; then
                        CITBX_JOB_DOCKER_RUN_ARGS+=(-v
                            $HOME/.docker/config.json:$HOME/.docker/config.json:${CITBX_DOCKER_AUTH_CONFIG_MODE##mount-})
                        if [ "$CITBX_UID" -ne 0 ]; then
                            CITBX_DOCKER_SCRIPT_PARTS+=("
                                # Fix permission on the $HOME/.docker
                                chown $CITBX_UID:$CITBX_UID $HOME/.docker
                            ")
                        fi
                    fi
                ;;
            esac
            CITBX_DOCKER_SCRIPT_PARTS+=("$CITBX_SCRIPT_COMMON"'
                # PATH environment variable propagation
                echo "ENV_PATH PATH=$PATH" >> /etc/login.defs
                echo "ENV_SUPATH PATH=$PATH" >> /etc/login.defs
            ')
            if [ "$CITBX_UID" -ne 0 ]; then
                CITBX_USER=$USER
                if [ "$CITBX_DOCKER_USER" == "root" ]; then
                    CITBX_DOCKER_SCRIPT_PARTS+=('
                        # _adduser <name> <uid> <home>
                        if _which useradd > /dev/null 2>&1; then
                            _adduser() {
                                useradd -o -u $2 -s /bin/sh -d $3 -M $1
                            }
                        elif _which busybox > /dev/null 2>&1; then
                            _adduser() {
                                busybox adduser -u $2 -s /bin/sh -h $3 -H -D $1
                            }
                        else
                            echo "[!!] No usual tool found to add an user"
                            exit 1
                        fi

                        # _adduser2group <user> <group>
                        if _which usermod > /dev/null 2>&1; then
                            _adduser2group() {
                                usermod -a -G $2 $1
                            }
                        elif _which busybox > /dev/null 2>&1; then
                            _adduser2group() {
                                addgroup $1 $2 > /dev/null
                            }
                        else
                            echo "[!!] No usual tool found to add an user to a group"
                            exit 1
                        fi

                        # Add the user
                        _adduser '"$CITBX_USER"' '"$CITBX_UID"' '"$HOME"'
                        # Fix rights on HOME directory
                        chown '"$CITBX_UID"':'"$CITBX_UID"' '"$HOME"'
                        # Add user to the suitable groups
                        for group in '"${CITBX_USER_GROUPS[*]}"'; do
                            if grep -q ^$group /etc/group; then
                                _adduser2group '"$CITBX_USER"' $group
                            fi
                        done
                        # Add sudoers suitable rule
                        if [ -f /etc/sudoers ]; then
                            sed -i "/^'"$CITBX_USER"' /d" /etc/sudoers
                            echo "'"$CITBX_USER"' ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
                        fi
                        ')
                fi
            else
                CITBX_USER=root
            fi
            # Shell selector (like the gitlab-runner one) will try to find bash, otherwise sh
            CITBX_DOCKER_SCRIPT_PARTS+=('su '"$CITBX_USER"' -s "$SHELL"')
            if [ "$CITBX_RUN_SHELL" != "true" ]; then
                CITBX_DOCKER_SCRIPT_PARTS+=("-c $CITBX_JOB_SCRIPT")
            fi

            if [ -n "$CITBX_DOCKER_USER" ]; then
                CITBX_JOB_DOCKER_RUN_ARGS+=(-u "$CITBX_DOCKER_USER")
            fi

            # Compute the environment variables
            for e in ${CITBX_ENV_EXPORT_LIST[@]}; do
                CITBX_DOCKER_RUN_ARGS+=(-e $e="${!e}")
            done

            CITBX_PRE_COMMANDS=()
            # Entrypoint override management
            if [ -n "$CITBX_DOCKER_ENTRYPOINT" ]; then
                CITBX_JOB_DOCKER_RUN_ARGS+=(--entrypoint "$CITBX_DOCKER_ENTRYPOINT")
                for e in "${CITBX_DOCKER_ENTRYPOINT[@]:1}"; do
                    CITBX_PRE_COMMANDS+=("$e")
                done
            fi

            # hook executed on exit
            executor_docker_exit_hook() {
                test -n "$CITBX_DOCKER_PREFIX" || print_critical "Assert: empty CITBX_DOCKER_PREFIX"
                for d in $(docker ps -a --filter "label=$CITBX_DOCKER_PREFIX" -q); do
                    docker rm -f $d > /dev/null 2>&1 || true
                done
            }
            trap executor_docker_exit_hook EXIT SIGHUP SIGINT SIGQUIT SIGABRT SIGKILL SIGALRM SIGTERM

            # Start a service
            start_docker_service() {
                local args=()
                local image=$1
                local name=${2:-$(echo "${image%%:*}" | sed -E 's/[^a-zA-Z0-9\._-]/__/g')}
                local ip
                local pattern='\b'"$name"'\b'
                if [[ "${CITBX_DISABLED_SERVICES[*]}" =~ $pattern ]]; then
                    print_note "Skipping $name service start"
                    return 0
                fi
                args+=(--name "$CITBX_DOCKER_PREFIX-$name" --label "$CITBX_DOCKER_PREFIX")
                shift 2
                if [ -n "$1" ]; then
                    args+=(--entrypoint "$1")
                fi
                if [ "$CITBX_SERVICE_DOCKER_PRIVILEGED" == "true" ]; then
                    args+=(--privileged)
                fi
                shift || true
                print_info "Starting service $name..."
                docker run -d "${args[@]}" "${CITBX_DOCKER_RUN_ARGS[@]}" "$image" "$@"
                # Get container IP and add --add-host options
                ip=$(docker inspect $CITBX_DOCKER_PREFIX-$name | jq -r .[0].NetworkSettings.Networks.bridge.IPAddress)
                if [ -z "$ip" ]; then
                    docker logs $CITBX_DOCKER_PREFIX-$name
                    print_critical "Unable to get the $name service IP address" \
                                    "This service seems to not be correctly started" \
                                    "=> Try again with --service-privileged option"
                fi
                CITBX_JOB_DOCKER_RUN_ARGS+=(--add-host "$name:$ip")
            }

            # Start services
            local start_docker_service_commands
            # Get the global and job service list from the .gitlab-ci.yml
            #   and extract additional properties like entrypoint if needed
            #   and generate bash subscript to start them using start_docker_service function
            eval "start_docker_service_commands=($(citbx_gitlab_ci_query '[[
                (."'"$CI_JOB_NAME"'" | if .services != null then .services[] else null end
                ), if .services != null then .services[] else null end
                ][] | select(. != null)] | map(
                    if (. | type) == "string" then
                        "start_docker_service \(@sh "\(.)") '\'\''"
                    else
                        "start_docker_service \(
                            if .name != null then @sh "\(.name)" else "'\'\''" end
                        ) \(if .alias != null then @sh "\(.alias)" else "'\'\''" end) \(
                            if .entrypoint != null and (.entrypoint | type)=="array" then
                                (.entrypoint | map(@sh "\(.)") | join(" "))
                            else ("'\'\''") end
                        ) \(if .command != null and (.command | type)=="array" then
                                (.command | map(@sh "\(.)") | join(" "))
                            else ("'\'\''") end
                        )"
                    end
                )[]'))"
            for cmd in "${start_docker_service_commands[@]}"; do
                eval "$cmd"
            done

            # Add project dir mount
            CITBX_JOB_DOCKER_RUN_ARGS+=(-v "$CI_PROJECT_DIR:$CI_PROJECT_DIR:rw")
            GIRDIR_PATH=$(readlink -f $(git rev-parse --git-common-dir))
            if [ "${GIRDIR_PATH#$CI_PROJECT_DIR}" == "$GIRDIR_PATH" ]; then
                # If the git dir is ouside the project dir
                CITBX_JOB_DOCKER_RUN_ARGS+=(-v "$GIRDIR_PATH:$GIRDIR_PATH:rw")
            fi

            if [ "$CITBX_JOB_ACCESS_TO_HOST_DOCKER_SOCK_ENABLED" == "true" ]; then
                CITBX_JOB_DOCKER_RUN_ARGS+=(-v /var/run/docker.sock:/var/run/docker.sock)
            fi

            if [ "$CITBX_RUN_SHELL" == "true" ]; then
                print_info "Running a shell into the $CITBX_DOCKER_IMAGE docker container..."
                CITBX_JOB_DOCKER_RUN_ARGS+=(-w "$PWD")
            else
                print_info "Running the job \"$CI_JOB_NAME\" into the $CITBX_DOCKER_IMAGE docker container..."
                CITBX_JOB_DOCKER_RUN_ARGS+=(-w "$CI_PROJECT_DIR")
            fi

            # Detect if the tool is launched from a terminal
            if tty -s; then
                CITBX_JOB_DOCKER_RUN_ARGS+=(-t)
            fi

            # Run the docker
            docker run --rm -i --name="$CITBX_DOCKER_PREFIX-build" --hostname="$CITBX_DOCKER_PREFIX-build" \
                -e CI=true -e GITLAB_CI=true "${CITBX_DOCKER_RUN_ARGS[@]}" \
                --label "$CITBX_DOCKER_PREFIX" "${CITBX_JOB_DOCKER_RUN_ARGS[@]}" \
                -e DOCKER_RUN_EXTRA_ARGS="$(bashopts_get_def bashopts_extra_args)" "${bashopts_extra_args[@]}" \
                $CITBX_DOCKER_IMAGE "${CITBX_PRE_COMMANDS[@]}" sh -c "${CITBX_DOCKER_SCRIPT_PARTS[*]}" \
                || exit $?
            ;;
        *)
            print_critical "Invalid or unsupported '$CITBX_JOB_EXECUTOR' executor"
            ;;
    esac
}

citbx_run_pipeline() {
    # hook executed on exit
    pipeline_exit_hook() {
        if [ $? -eq 0 ]; then
            print_info "Pipeline succeed"
        else
            print_error "Pipeline failed"
        fi
    }
    trap pipeline_exit_hook EXIT SIGHUP SIGINT SIGQUIT SIGABRT SIGKILL SIGALRM SIGTERM
    local job_list
    if [ -z "$CITBX_PIPELINE_NAME" ]; then
        if [ "$CITBX_DISABLE_DEFAULT_GLOBAL_PIPELINE" == "true" ]; then
            print_critical "Default global pipeline is disabled for this project" \
                           " => You can use -n|--pipeline-name option to specify a pipeline name"
        fi
        print_info "Running the default global pipeline..."
        # Get the stage list from the .gitlab-ci.yml file
        local stage_list
        eval "stage_list=($(citbx_gitlab_ci_query '
            (if (.stages | type) == "array" then .stages
            else ["build", "test", "deploy"] end)[]'))"
        for stage in "${stage_list[@]}"; do
            # For each stage, in the good order,
            #   get the job list from the gitlab-ci.yml file
            eval "job_list+=($(citbx_gitlab_ci_query '
                to_entries[] | select((.value | type) == "object"
                    and (.key | split(".")[0]) != ""
                    and .value.stage == "'"$stage"'") | .key'))"
        done
    else
        print_info "Running the pipeline '$CITBX_PIPELINE_NAME'..."
        # Get the job list for the selected pipeline from the .ci-pipelines.yml
        eval "job_list=($(citbx_pipelines_query '."'"$CITBX_PIPELINE_NAME"'" | .[]
            | if (. | type) == "object" then .name else . end '))"
    fi
    for job in "${job_list[@]}"; do
        local job_arg_list
        # For each job,of the selected pipeline,
        #   extract the additional arguments from the .ci-pipelines.yml
        eval "job_arg_list=($(citbx_pipelines_query '
            if (."'"$CITBX_PIPELINE_NAME"'" | type) == "array" then
                ."'"$CITBX_PIPELINE_NAME"'"[]
                | select((. | type)=="object" and .name=="'"$job"'")
                | .arguments | to_entries
                | map(
                    if (.value | type) == "array" then
                        "\(.key as $key | .value
                            | map("--\($key)", "\(.)") | .[])"
                    else
                        "--\(.key)", "\(.value)"
                    end
                )
            else [] end | .[]'))"
        "$0" "$job" "${job_arg_list[@]}"
    done
}

# display a message
print_log() {
    local level=$1;
    shift || print_log C "Usage print_log <level> message"
    case "${level,,}" in
        c|critical)
            >&2 printf "\033[91m[CRIT] %s\033[0m\n" "$@"
            exit 1
            ;;
        e|error)
            >&2 printf "\033[91m[ERRO] %s\033[0m\n" "$@"
            ;;
        w|warning)
            >&2 printf "\033[93m[WARN] %s\033[0m\n" "$@"
            ;;
        n|note)
            printf "[NOTE] %s\n" "$@"
            ;;
        i|info)
            printf "\033[92m[INFO] %s\033[0m\n" "$@"
            ;;
        *)
            print_log C "Invalid log level: $level"
            ;;
    esac
}
bashopts_log_handler="print_log"
. "$CITBX_TOOL_DIR/3rdparty/bashopts.sh"

if [ "$CITBX_BACKTRACE_ENABLED" == "true" ]; then
    # Enable backtrace display on error
    trap 'bashopts_exit_handle' EXIT SIGHUP SIGINT SIGQUIT SIGABRT SIGKILL SIGALRM SIGTERM
fi

# Set the setting file path
if [ -z "$CITBX_RC_PATH" ]; then
    CITBX_RC_PATH="/dev/null"
fi

if [ ${#CITBX_MISSING_PKGS[@]} -eq 0 ]; then
    CITBX_GITLAB_CI_JSON=$(citbx_yaml2json $CI_PROJECT_DIR/${CI_CONFIG_PATH:-.gitlab-ci.yml})
    CITBX_JOB_LIST=()
    # Fill CITBX_JOB_LIST array
    eval "$(citbx_gitlab_ci_query -r '[. | to_entries[]
        | select((.value | type) == "object"
            and (.value.script | type) == "array" and .key[0:1] != ".") 
        | .key] | map(@sh "CITBX_JOB_LIST+=(\(.));")[]')"
fi

if [ "$CITBX_OPTION_INTERACTIVE_MODE_ENABLED_DEFAULT" != "true" ]; then
    # Set non interactive mode by default
    BASHOPTS_SETUP_OPTS+=(-y)
fi

bashopts_setup -n "$(basename ci-toolbox)" \
    -d "CI toolbox for Gitlab-CI (version $CITBX_VERSION)" \
    -s "$CITBX_RC_PATH" "${BASHOPTS_SETUP_OPTS[@]}"

if [ "$CITBX_BASHCOMP" == "commands" ]; then
    printf '"%s"\n' help setup docker-prune pipeline "${CITBX_JOB_LIST[@]}"
    exit 0
fi

command=$1
shift || true
case "$command" in
    ''|h|help|-h|--help)
        bashopts_tool_usage="ci-toolbox command [command options] [arguments...]
  => type 'ci-toolbox command -h' to display the contextual help

COMMANDS:
    help         : Display this help
    setup        : Setup the environment
    docker-prune : Perform a docker image & volume prune
    pipeline     : Run the global or a specific pipeline
    ... or a job from the job list

JOBS:
$(for j in "${CITBX_JOB_LIST[@]}"; do echo "    $j"; done | sort -u)"
        bashopts_display_help_delayed
        ;;
    setup)
        bashopts_tool_usage="ci-toolbox $command [arguments...]
  => type 'ci-toolbox help' to display the global help"
        CITBX_SETUP_COMPONENT_DEFAULT_LIST+=(base-pkgs docker-cfg ca-certs)
        bashopts_declare -n CITBX_SETUP_COMPONENT -l component -i \
            -t enum -m add -d "Setup only specified components" \
            -e base-pkgs -e docker-cfg -e git-lfs -e ca-certs -e ci-toolbox \
            -x "(${CITBX_SETUP_COMPONENT_DEFAULT_LIST[*]})"
        bashopts_declare -n CITBX_SETUP_OS_ID -l os-id -i \
            -t string -d "Operating system ID" \
            -v "${OS_RELEASE_INFO[NAME]}"
        bashopts_declare -n CITBX_DOCKER_BIP -l docker-bip -i -v "$(
            val=$(jq -r '.bip' /etc/docker/daemon.json 2> /dev/null || true)
            echo ${val:-"192.168.255.254/24"}
        )" -t string -d "Local docker network IPV4 host adress"
        bashopts_declare -n CITBX_DOCKER_FIXED_CIDR -l docker-cdir -i -v "$(
            val=$(jq -r '."fixed-cidr"' /etc/docker/daemon.json 2> /dev/null || true)
            echo ${val:-"192.168.255.0/24"}
        )" -t string -d "Local docker network IPV4 prefix"
        bashopts_declare -n CITBX_DOCKER_DNS_LIST -l docker-dns -i -m add \
            -x "($(
                if [ "0$(jq -e '.dns | length' /etc/docker/daemon.json 2> /dev/null || true)" -gt 0 ]; then
                    jq -r '.dns[]' /etc/docker/daemon.json 2> /dev/null | tr '\n' ' '
                else
                    RESOLV_CONF_DNS="$(cat /etc/resolv.conf | awk '/^nameserver/ {
                        if ($2 !~ /^127\..*/ && $2 != "::1" ) {
                            printf(" %s", $2);
                        }
                    }' 2> /dev/null || true)"
                    echo "${RESOLV_CONF_DNS:-${CITBX_DOCKER_DEFAULT_DNS[*]}}"
                fi
                ) )" \
            -t string -d "Docker DNS"
        bashopts_declare -n CITBX_DOCKER_STORAGE_DRIVER -l docker-storage-driver -i -v "$(
            val=$(jq -r '."storage-driver"' /etc/docker/daemon.json 2> /dev/null || true)
            echo ${val:-"overlay2"}
        )" -e 'o|overlay2' -e 'overlay' -e 'a|aufs' -e 'd|devicemapper' -e 'b|btrfs' -e 'z|zfs' \
            -t enum -d "Docker storage driver"
        bashopts_declare -n CITBX_CA_CERTIFICATES -l ca-certificates -i -t string \
            -d "CA certificate directory with certificates (file names with '.crt' extension) to import"
        ;;
    docker-prune)
        ;;
    pipeline)
        if [ -z "$CI_PROJECT_DIR" ]; then
            print_critical "Unable to detect project root directory"
        fi
        bashopts_tool_usage="ci-toolbox pipeline [arguments...]
  => type 'ci-toolbox help' to display the global help"
        CITBX_PIPELINES_CONFIG_PATH=${CITBX_PIPELINES_CONFIG_PATH:-.ci-pipelines.yml}
        if [ -e "$CI_PROJECT_DIR/$CITBX_PIPELINES_CONFIG_PATH" ]; then
            if [[ "$1" =~ ^[^-] ]]; then
                CITBX_PIPELINE_NAME=$1
            fi
            CITBX_PIPELINES_JSON=$(citbx_yaml2json "$CI_PROJECT_DIR/$CITBX_PIPELINES_CONFIG_PATH")
            eval "bashopts_declare_opts=($(citbx_pipelines_query  -r '. | keys | map(@sh "-e \(.)")[]'))"
            bashopts_declare -n CITBX_PIPELINE_NAME -l pipeline-name -o n -t enum  \
                -e '' "${bashopts_declare_opts[@]}" \
                -v "$CITBX_DEFAULT_PIPELINE_NAME" -d "Select the pipeline name to run"
        fi
        ;;
    *)
        if [ -z "$CI_PROJECT_DIR" ]; then
            print_critical "Unable to detect project root directory"
        fi
        if [ ! -e "$CI_PROJECT_DIR/$CI_CONFIG_PATH" ]; then
            print_critical "$CI_PROJECT_DIR/$CI_CONFIG_PATH file not found"
        fi
        # Command check
        if ! printf "%s\n" "${CITBX_JOB_LIST[@]}" | grep -q ^"$command"$; then
            print_critical "Unreconized command '$command'; type 'ci-toolbox help' to display the help"
        fi
        CI_JOB_NAME=$command
        # Read job specific and global image property from the .gitlab-ci.yml
        #   and read additional property like entrypoint if needed
        #   and generate the script to initialize the suitable bash variable
        eval "$(citbx_gitlab_ci_query -r '
            [[."'"$CI_JOB_NAME"'".image , .image][]
                | select(.!=null)]
                | if (. | length) != 0 then
                    .[0] | if (. | type) == "string" then
                        @sh "CITBX_DEFAULT_DOCKER_IMAGE=\(.)"
                    elif (. | type) == "object" then
                        "CITBX_DEFAULT_DOCKER_IMAGE=\(@sh "\(.name)")
                        CITBX_DEFAULT_DOCKER_ENTRYPOINT=(\(.entrypoint | map(@sh "\(.)") | join(" ")))"
                    else "" end
                else "" end')"
        # Find invalid variable declaration from the global and job specific variables property
        eval "invalid_vars=($(citbx_gitlab_ci_query '[
            [[.variables,."'"$CI_JOB_NAME"'".variables,{}][] | select (. != null)] | add | to_entries[]
            | select ((.value | type) != "string" and (.value | type) != "number")]
            | map("* invalid variable \(.key) (type: \(.value | type))")[]'))"
        if [ ${#__invalid_vars[@]} -ne 0 ]; then
            print_critical "Only strings and numbers are supported:" "${__invalid_vars[@]}"
        fi
        unset __invalid_vars
        # Transform variable declaration from the global and job specific variables property
        #   to a bash script to forward these to the suitable contextes
        eval "$(citbx_gitlab_ci_query -r '[[.variables,."'"$CI_JOB_NAME"'".variables,{}][]
            | select (. != null)] | add | to_entries
            | map(@sh "
                CITBX_JOB_VARIABLE_LIST+=(\(.key));
                eval \(.key)=\(.value|tojson);
                eval CITBX_DOCKER_RUN_ARGS+=(-e \("\(.key)=\(.value)"|tojson));
            ") | join("")')"
        # NOTE: Assure backward compatibility (old name for CITBX_JOB_FILE_NAME property)
        CITBX_JOB_FILE_NAME=${CITBX_JOB_FILE_NAME:-${CITBX_JOB_RUN_FILE_NAME:-"$CI_JOB_NAME.sh"}}
        # Define job usage
        bashopts_tool_usage="ci-toolbox '${command//\'/\\\'}' [arguments...]
  => type 'ci-toolbox help' to display the global help"
        # Define the generic options
        bashopts_declare -n GIT_SUBMODULE_STRATEGY -l submodule-strategy \
            -d "Git submodule strategy (none, normal or recursive)" -t enum -v "${GIT_SUBMODULE_STRATEGY:-none}" \
            -e 'none' -e 'normal' -e 'recursive'
        bashopts_declare -n CITBX_GIT_CLEAN -l git-clean -o c \
            -d "Perfom a git clean -fdx in the main project and submodules" -t boolean
        declare_opts=()
        if [ -n "$DEFAULT_CI_REGISTRY" ]; then
            declare_opts+=(-v "$DEFAULT_CI_REGISTRY")
        fi
        bashopts_declare -n CI_REGISTRY -l docker-registry -d "Docker registry" -t string -s "${declare_opts[@]}"
        unset declare_opts
        bashopts_declare -n CITBX_DOCKER_LOGIN_MODE -l docker-login -d "Execute docker login" -t enum \
            -e "enabled" -e "disabled" -e "auto" -v "${CITBX_DEFAULT_DOCKER_LOGIN_MODE:-disabled}"
        bashopts_declare -n CITBX_JOB_EXECUTOR -l job-executor -o e \
            -d "Job executor type (only docker or shell is supported yet)" -t enum \
            -v "$(test -n "$CITBX_DEFAULT_DOCKER_IMAGE" && echo "docker" || echo "shell" )" \
            -e 's|shell' -e 'd|docker'
        bashopts_declare -n CITBX_RUN_JOB_DEPENDENCIES -l with-dependencies -t boolean \
            -d "Run job dependencies"
        bashopts_declare -n CITBX_SUBJOB_ENV_EXPORT_LIST -l export-to-subjob -o E -t string -m add \
            -d "Export a property to a sub job (only applicable if CITBX_RUN_JOB_DEPENDENCIES property/export-to-subjob option is enabled)"
        bashopts_declare -n CITBX_DOCKER_IMAGE -l docker-image -d "Docker image name" -t string \
            -x "\"$CITBX_DEFAULT_DOCKER_IMAGE\""
        bashopts_declare -n CITBX_DOCKER_ENTRYPOINT -l docker-entrypoint -d "Docker entrypoint" -t string -m add \
            -x "$(bashopts_get_def CITBX_DEFAULT_DOCKER_ENTRYPOINT)"
        bashopts_declare -n CITBX_UID -l uid -t number \
            -d "Start this script as a specific uid (0 for root)" -v "$(id -u)"
        CITBX_USER_GROUPS=(adm plugdev)
        bashopts_declare -n CITBX_USER_GROUPS -l group -t string -m add \
            -d "User group list"
        bashopts_declare -n CITBX_DEBUG_SCRIPT_ENABLED -o x -l debug-script -t boolean \
            -d "Enable SHELL script debug (set -e)"
        citbx_export CITBX_DEBUG_SCRIPT_ENABLED
        bashopts_declare -n CITBX_RUN_SHELL -o s -l shell -t boolean \
            -d "Run a shell instead of running the default command (override CITBX_COMMAND option)"
        bashopts_declare -n CITBX_DISABLED_SERVICES -l disable-service -t string \
            -d "Disable a service" -m add
        bashopts_declare -n CITBX_SERVICE_DOCKER_PRIVILEGED -l service-privileged -t boolean \
            -d "Start service docker container in privileged mode" -v "${CITBX_DEFAULT_SERVICE_DOCKER_PRIVILEGED:-false}"
        bashopts_declare -n CITBX_JOB_ACCESS_TO_HOST_DOCKER_SOCK_ENABLED -l access-to-host-docker -t boolean \
            -d "Enable access to the host docker daemon into the job environment" -v "${CITBX_DEFAULT_JOB_ACCESS_TO_HOST_DOCKER_SOCK_ENABLED:-false}"
        bashopts_declare -n DOCKER_AUTH_CONFIG -l docker-auth-config -t string \
            -d "Docker config file (default: $HOME/.docker/config.json) content" -v "$(
                if [ -f "$HOME/.docker/config.json" ]; then
                    jq -c '' "$HOME/.docker/config.json"
                fi
            )"
        bashopts_declare -n CITBX_DOCKER_AUTH_CONFIG_MODE -l docker-auth-config-mode -t enum -e none -e environment -e mount-ro -e mount-rw \
            -d "Docker configuration can be propagated using DOCKER_AUTH_CONFIG variable, or else by mounting your $HOME/.docker/config.json" \
            -v "${CITBX_DEFAULT_DOCKER_AUTH_CONFIG_MODE:-none}"
        CITBX_DOCKER_USER=${CITBX_DOCKER_USER:-root}

        # Load job
        module_handler_list="define setup"
        if [ -f "$CITBX_JOBS_DIR/$CITBX_JOB_FILE_NAME" ]; then
            . "$CITBX_JOBS_DIR/$CITBX_JOB_FILE_NAME"
            citbx_register_handler "job" "define"
            citbx_register_handler "job" "setup"
        fi
        for hook in $citbx_job_stage_define; do
            cd $CI_PROJECT_DIR
            $hook
        done
        ;;
esac

if [ -n "$CITBX_BASHCOMP" ]; then
    # ### BASH completion specific part ###
    # Used only by the bashcomp tool to generate completion words
    case "$CITBX_BASHCOMP" in
        opts)
            for o in "${bashopts_optprop_short_opt[@]}"; do
                echo "\"-$o\""
            done | sort -u
            for o in "${bashopts_optprop_long_opt[@]}"; do
                echo "\"--$o\""
            done | sort -u
            ;;
        longopts)
            for o in "${bashopts_optprop_long_opt[@]}"; do
                echo "\"--$o\""
            done | sort -u
            ;;
        --docker-image)
            while read -r line; do
                echo "\"$line\""
            done <<< "$(docker image ls | tail -n +2 \
                | awk '($1 != "<none>" && $2 != "<none>") {print $1":"$2}')"
            ;;
        --export-to-subjob|-E)
            printf '"%s"\n' "${bashopts_optlist[@]:1}"
            ;;
        -*)
            bashopts_get_valid_value_list $CITBX_BASHCOMP
            ;;
    esac
    exit 0
fi

# Parse arguments
bashopts_parse_args "$@"

# Process argument
bashopts_process_opts

# check the environment
citbx_check_env $(test "$command" != "setup" || echo "true")

case "$command" in
    docker-prune)
        docker image prune -f
        docker volume prune -f
        exit 0
    ;;
    pipeline)
        citbx_run_pipeline
    ;;
    *)
        citbx_run_job "$command"
    ;;
esac
