
citbx_module_runshell_define() {
    bashopts_declare -n CITBX_RUN_SHELL_ON -l run-shell-on -v none \
        -t enum -e none -e startup -e failure -e success -e finish \
        -d "Run a shell on a specific build step, useful to debug"
    citbx_export CITBX_RUN_SHELL_ON
}

citbx_module_runshell_before() {
    if [ "$CITBX_RUN_SHELL_ON" == "startup" ]; then
        print_info "Starting a shell command line before the job payload" \
                    "=> Type 'exit' to continue or 'exit 1' to abort"
        bash
    fi
}

citbx_module_runshell_after() {
    if [ $1 -eq 0 ]; then
        if [[ $CITBX_RUN_SHELL_ON =~ success|finish ]]; then
            print_info "Starting a shell command line after the job success"
            bash
        fi
    else
        if [[ $CITBX_RUN_SHELL_ON =~ failure|finish ]]; then
            print_info "Starting a shell command line after the job failure"
            bash
        fi
    fi
}
